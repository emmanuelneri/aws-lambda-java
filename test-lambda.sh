#!/usr/bin/env bash

gatewayUrl=https://w1d25jn9x6.execute-api.us-east-1.amazonaws.com/default
functionPath=/helloLambda
url=${gatewayUrl}${functionPath}

request="{\"name\":\"Teste\", \"email\":\"teste@gmail.com\"}"


curl -d "${request}" -H "Content-Type: application/json" -X POST ${url}/request