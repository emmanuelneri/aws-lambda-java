package br.com.emmanuelneri;

import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.Context;

public class HelloLambda implements RequestHandler<Request, Response> {

    public Response handleRequest(final Request request, final Context context) {
        return new Response(request.getName(), request.getEmail());
    }
}
