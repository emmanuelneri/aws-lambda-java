package br.com.emmanuelneri;

public class Response {

    private final String body;

    Response(final String name, final String email) {
        this.body = String.format("Request received: Name: %s, email: %s.", name, email);
    }

    public String getBody() {
        return body;
    }
}
